﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChangeRoom : MonoBehaviour {

    public int nextRoom;
    public int previousRoom;
    private bool notChangeRoomGuardian;
    private bool notChangeRoomHero;
    public bool activeOnLeft;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Hero" && nextRoom != collision.GetComponent<HeroStatus>().currentInRoom)
        {
            if (!notChangeRoomHero && ((activeOnLeft && collision.gameObject.transform.position.x < transform.position.x) || (!activeOnLeft && collision.gameObject.transform.position.x > transform.position.x)))
            {
               // print("hero from " + collision.GetComponent<HeroStatus>().currentInRoom);
                collision.GetComponent<HeroStatus>().currentInRoom = nextRoom;
               // print("hero to " + collision.GetComponent<HeroStatus>().currentInRoom);

            }

            notChangeRoomHero = false;
        }

        

        if (collision.gameObject.tag == "Guardian" && nextRoom != collision.GetComponent<GuardianFSM>().currentInRoom)
        {
            if (!notChangeRoomGuardian && ((activeOnLeft && collision.gameObject.transform.position.x < transform.position.x) || (!activeOnLeft && collision.gameObject.transform.position.x > transform.position.x)))
            {
                //print("Enemy from " + collision.GetComponent<GuardianFSM>().currentInRoom);
                collision.GetComponent<GuardianFSM>().currentInRoom = nextRoom;
                //print("Enemy to " + collision.GetComponent<GuardianFSM>().currentInRoom);
            }

            notChangeRoomGuardian = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "Hero" && nextRoom == collision.GetComponent<HeroStatus>().currentInRoom)
        {
            notChangeRoomHero = true;
        }

        if(collision.gameObject.tag == "Guardian" && nextRoom == collision.GetComponent<GuardianFSM>().currentInRoom)
        {
            notChangeRoomGuardian = true;
        }

    }

}
