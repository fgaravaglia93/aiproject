﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateState : MonoBehaviour {

    public bool locked = false;

    private void Start()
    {
        //set all the doors open
        if(transform.childCount>0)
            transform.GetChild(0).gameObject.SetActive(true);
        GetComponent<Animator>().SetBool("GateLock", GetComponent<GateState>().locked);
        Destroy(GetComponent<BoxCollider2D>());
    }
    public void RefreshCollider()
    {

        
        if(locked)
        {
            gameObject.AddComponent<BoxCollider2D>();
            transform.GetChild(0).gameObject.SetActive(false);
            
        }
        else
        {
            Destroy(GetComponent<BoxCollider2D>());
            transform.GetChild(0).gameObject.SetActive(true);
        }
        
    }

}