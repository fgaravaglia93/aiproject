﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//<summary>
//Highlight the object if player has passed over it with mouse
//</summary>
public class MovableObj : MonoBehaviour
{

    private Color clr;
    //private bool movable = false;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseEnter()
    {
        clr = GetComponent<SpriteRenderer>().color;
    }

    private void OnMouseOver()
    {
        if (GetComponent<SpriteRenderer>().color.r > 0.7f)
        {
            GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r - 0.05f, GetComponent<SpriteRenderer>().color.g - 0.025f, GetComponent<SpriteRenderer>().color.b);
        }
    }

    private void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().color = clr;
 
    }

    /*public void SetMovable(bool movable)
    {
        this.movable = movable; 
    }*/
}
