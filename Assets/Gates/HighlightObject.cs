﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightObject : MonoBehaviour {

    // Use this for initialization
    public Sprite HighlightSprite;
    private Sprite Sprite;

    void Start()
    {
        Sprite = GetComponent<SpriteRenderer>().sprite;
    }
	void OnMouseDown()
    {
        GetComponent<SpriteRenderer>().sprite = HighlightSprite;

    }

    void OnMouseUp()
    {
        GetComponent<SpriteRenderer>().sprite = Sprite;
    }



}
