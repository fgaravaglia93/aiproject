﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowControls : MonoBehaviour {
    public GameObject controls;
    // Use this for initialization
    void Start()
    {
        controls.SetActive(false);
    }

    void Update()
    {
        if (Input.GetButtonDown("Escape"))
        {
            controls.SetActive(false);
        }
    }
    public void ShowControl()
    {
        controls.SetActive(true);
    }
}
