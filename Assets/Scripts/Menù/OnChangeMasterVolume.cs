﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OnChangeMasterVolume : MonoBehaviour {

    public Slider mySlider;

    public void MasterVolumeChanged()
    {

        AudioListener.volume = mySlider.value;
    }
}
