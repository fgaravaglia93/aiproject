﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnChangeMusicVol : MonoBehaviour {

    public Slider mySlider;
    public AudioSource musicSource;

    public void ChangeMusicVolume()
    {

        musicSource.volume = mySlider.value;
    }

}
