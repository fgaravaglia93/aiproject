﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridPathFinding : MonoBehaviour
{

    public GameObject tileGrid;


    //public TileNode[] path;

    public bool stopAtFirstHit = false;

    
    private Vector2 gridWorldSize;
    private float nodeRadius;
    public bool followPath = false;
    private TileNode[,] grid;
    private List<TileNode> path, reversePath;
    
    private int gridSizeX, gridSizeY;

  
    private Vector3 currentPositionHolder;
    public float speed;
    private int currentNode;
    private float distance;
    private float finalSpeed;

  //facing
  [HideInInspector]
    public bool facingRight = true;
    private float prevPos;
    private Quaternion theRotation;
    // Use this for initialization
    void Start()
    {
        prevPos = transform.position.x;
        grid = transform.parent.GetComponent<TileGridGenerator>().GetGrid();
    }

    private void Update()
    {
        if (followPath)
        {
            
            if((transform.position - currentPositionHolder).magnitude>0.5f)
            {
                prevPos = transform.position.x;
                distance = Vector3.Distance(transform.position, currentPositionHolder);
                finalSpeed = (distance / speed);
                transform.position = Vector3.Lerp(transform.position, currentPositionHolder, Time.deltaTime / finalSpeed);
                
                if (!facingRight && transform.position.x > prevPos || facingRight && transform.position.x < prevPos)
                {
                    theRotation = transform.rotation;
                    theRotation.y += 180;
                    transform.rotation *= theRotation;
                    facingRight = !facingRight;
                }

            }
            else
            {
              if (currentNode < path.Count - 1)
                {
                    CheckNode();
                    currentNode++;
                }
            }
        }
    }

    void CheckNode()
    {
        //smoothing go the farther visible node of the path
        int i = 0;
        
        foreach (TileNode n in reversePath)
        {

           // Debug.DrawRay(transform.position, (n.worldPosition - (Vector2)transform.position), Color.green, 2f);
            RaycastHit2D hit = Physics2D.Linecast(transform.position, n.worldPosition, LayerMask.GetMask("Ground"));
            //print(hit.collider.gameObject.name);
            if (hit.collider == null)
            {
                currentPositionHolder = n.worldPosition;

               
                break;
            }
            i++;
        }
    }

    public void Chase(Transform target)
    {

        path = new List<TileNode>();

        path = GetPath(transform, target);
        
        tileGrid.GetComponent<TileGridGenerator>().SetGizmosPath(path);
        reversePath = path;
        reversePath.Reverse();
        if(path.Count>0)
            currentPositionHolder = path.ToArray()[0].worldPosition;
        currentNode = 0;

        CheckNode();

        followPath = true;

    }

    public List<TileNode> GetPath(Transform start, Transform end)
    {
        List<TileNode> aStarPath;
        TileNode nodeStart = transform.parent.GetComponent<TileGridGenerator>().NodeFromWorldPoint(start.position);
        TileNode nodeTarget = transform.parent.GetComponent<TileGridGenerator>().NodeFromWorldPoint(end.position);
        gridSizeX = transform.parent.GetComponent<TileGridGenerator>().GetSizeX();
        gridSizeY = transform.parent.GetComponent<TileGridGenerator>().GetSizeY();
        aStarPath = AStarSolverGrid.Solve(transform.parent.GetComponent<TileGridGenerator>().GetGrid(), gridSizeX, gridSizeY, nodeStart, nodeTarget, EuclideanEstimator);

        return aStarPath;
    }

    float EuclideanEstimator(TileNode from, TileNode to)
    {
        return (from.worldPosition - to.worldPosition).magnitude;
    }

    

    
}