﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomsConnections : MonoBehaviour{

	private Dictionary<int, List<RoomNode>> roomsAdjacents = new Dictionary<int, List<RoomNode>>();

    private GameObject[] nodesObj;

	void Start()
    {
        int n = transform.childCount;
        nodesObj = new GameObject[n];

        for (int i = 0; i < transform.childCount; i++)
        {
            nodesObj[i] = transform.GetChild(i).gameObject;
        }

        List<RoomNode> adjacents;

        adjacents = new List<RoomNode> {
            new RoomNode(2, nodesObj[1])
        };
        roomsAdjacents.Add(1, adjacents);

        adjacents = new List<RoomNode>()
        {
            new RoomNode(1, nodesObj[0]),
            new RoomNode(3, nodesObj[2]),
            new RoomNode(4, nodesObj[3])
        };
        roomsAdjacents.Add(2, adjacents);

        adjacents = new List<RoomNode> {
            new RoomNode(5, nodesObj[4]),
            new RoomNode(2, nodesObj[1]),
            new RoomNode(4, nodesObj[3])

        };
        roomsAdjacents.Add(3, adjacents);

        adjacents = new List<RoomNode>()
        {
            new RoomNode(2, nodesObj[1]),
            new RoomNode(3, nodesObj[2]),
            new RoomNode(5, nodesObj[4]),
        };
        roomsAdjacents.Add(4, adjacents);

        adjacents = new List<RoomNode>()
        {
            new RoomNode(3, nodesObj[2]),
            new RoomNode(4, nodesObj[3]),
        };
        roomsAdjacents.Add(5, adjacents);
    }

    public RoomNode[] GetAdjacents(int key)
    {
        List<RoomNode> adjacents = null;
        roomsAdjacents.TryGetValue(key, out adjacents);
        return adjacents.ToArray();
        
    }
}
