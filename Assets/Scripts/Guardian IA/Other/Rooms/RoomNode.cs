﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomNode {


    
    public int roomIndex;
    public GameObject sceneObject;

    public RoomNode(int roomIndex, GameObject o = null)
    {
        this.roomIndex = roomIndex;
        this.sceneObject = o;
    }

}
