﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitToPick : MonoBehaviour {

    public bool isPickable = false;
    public float waitingTimeForCollider;

	
    public IEnumerator CanBePickable()
    {
        yield return new WaitForSeconds(waitingTimeForCollider);
        isPickable = true;
    }
	
	
	
}
