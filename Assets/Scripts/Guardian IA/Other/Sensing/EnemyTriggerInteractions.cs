﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTriggerInteractions : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Crystal" && collision.gameObject.GetComponent<WaitToPick>().isPickable)
        {
            print("Retrieve del cristallo");
            transform.parent.GetComponent<GuardianFSM>().havingCrystal = true;
            Destroy(collision.gameObject);
        }


    }
}
