﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerClear : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Gate" && collision.gameObject.transform.parent.tag == "Opengate")
        {
            print("gate found");
            transform.parent.gameObject.GetComponent<GuardianFSM>().foundGate = true;
            transform.parent.gameObject.GetComponent<GuardianFSM>().gate= collision.gameObject;
        }
    }
	
}
