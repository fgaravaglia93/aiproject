﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianAttackTrigger : MonoBehaviour {

    private bool inAttack = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //avoid trigger because of hero in another room
        if (collision.gameObject.tag == "Hero")
        {
            
            if (collision.gameObject.GetComponent<HeroStatus>().id == "Alchemist")
                transform.parent.GetComponent<CRBTGuardianAttack>().alchTarget = collision.gameObject;
            if (collision.gameObject.GetComponent<HeroStatus>().id == "Telekinetic")
                transform.parent.GetComponent<CRBTGuardianAttack>().telTarget = collision.gameObject;


        }
            


     
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!inAttack && collision.gameObject.tag == "Hero" && collision.gameObject.GetComponent<HeroStatus>().currentInRoom == transform.parent.GetComponent<GuardianFSM>().currentInRoom )
        {
            transform.parent.GetComponent<GuardianFSM>().heroClose = true;
            inAttack = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Hero")
        {
            
            //transform.parent.GetComponent<GuardianFSM>().heroClose = false;
            if (collision.gameObject.GetComponent<HeroStatus>().id == "Alchemist")
                transform.parent.GetComponent<CRBTGuardianAttack>().alchTarget = null;
            if (collision.gameObject.GetComponent<HeroStatus>().id == "Telekinetic")
                transform.parent.GetComponent<CRBTGuardianAttack>().telTarget = null;
            transform.parent.GetComponent<GuardianFSM>().heroClose = !(transform.parent.GetComponent<CRBTGuardianAttack>().telTarget == null && transform.parent.GetComponent<CRBTGuardianAttack>().alchTarget == null);
            inAttack = transform.parent.GetComponent<GuardianFSM>().heroClose;
        }
    }
}
