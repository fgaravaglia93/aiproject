﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianShoot : MonoBehaviour
{

    public GameObject bulletStraightPrefab;


    [SerializeField]
    private Transform bulletSpawn;
    [SerializeField]
    private Transform bullet2Spawn;

    [SerializeField]
    private GameObject gun;
    [SerializeField]
    private GameObject gun2;


    public bool missed = false;
    // Use this for initialization
    void Start()
    {

        gun = transform.GetChild(0).gameObject;
        bulletSpawn = gun.transform.GetChild(0);
    }

    public void FireStraight(Vector3 target)
    {
        float angle = Mathf.Atan2((target - bulletSpawn.position).y, (target - bulletSpawn.position).x) * Mathf.Rad2Deg;

        var bullet = Instantiate(bulletStraightPrefab, bulletSpawn.position, Quaternion.Euler(0, 0, angle));
        bullet.GetComponent<GuardianBullet>().bt = GetComponent<CRBTGuardianAttack>();
    }



    public void FireCircular(int nBullets)
    {
        float angleStep = 360f / nBullets;
        float angle = 0f;
        float dirX, dirY;
        float radius = 1f;
        RaycastHit2D hit;
        for (int i = 0; i < nBullets; i++)
        {
            dirX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180)*radius;
            dirY = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180) * radius;
            //Debug.DrawRay(transform.position, new Vector2(dirX, dirY)* radius, Color.red, 0.5f);
            hit = Physics2D.Linecast(transform.position, new Vector2(dirX,dirY)*radius ,LayerMask.GetMask("Ground"));
            if (hit.collider  == null)
            {
                var bullet = Instantiate(bulletStraightPrefab, transform.position, Quaternion.Euler(0, 0, angle));
                bullet.GetComponent<GuardianBullet>().bt = GetComponent<CRBTGuardianAttack>();
            }           
            angle += angleStep;
        }
     
    }
}



