﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CRBT;

public class GuardianBullet : MonoBehaviour
{

    public float speed = 10;
    public float damage = .3f;
    public bool missed = false;
    public CRBTGuardianAttack bt;
    public GameObject crystalPrefab;

    private Vector2 force;
    private GameObject crystal;
    Collision2D other;

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Hero")
        {
            missed = false;
            //Debug.Log("Hero Beccato!");
            
            other.gameObject.GetComponent<ManaBar>().healthBarSlider.value -= 0.33f;
            if (other.gameObject.GetComponent<HeroStatus>().haveCrystal && other.gameObject.GetComponent<ManaBar>().healthBarSlider.value<0.32)
            {
                this.other = other;
                //print("ciao perdo il cristallo");
                other.gameObject.GetComponent<HeroStatus>().haveCrystal = false;
                Vector3 spawnPos = new Vector3(other.gameObject.transform.position.x,
                                               other.gameObject.transform.position.y + 1f,
                                               other.gameObject.transform.position.z);
                    
                crystal = Instantiate(crystalPrefab, spawnPos, Quaternion.identity);
                bt.gameObject.GetComponent<GuardianFSM>().crystal = crystal;
                LaunchCrystal(crystal);
                crystal.GetComponent<WaitToPick>().StartCoroutine("CanBePickable");
                bt.GetComponent<CRBTGuardianAttack>().StartCoroutine("WaitToSeeCrystal");
                GameObject otherHero = other.gameObject.GetComponent<HeroStatus>().otherHero;
            } else if (!other.gameObject.GetComponent<HeroStatus>().haveCrystal)
            {
                other.gameObject.GetComponent<HeroStatus>().alive = false;
            }

            Destroy(gameObject);
        }

        else if (other.gameObject.tag == "Ground")
        {
            missed = true;
            Destroy(gameObject);
        }

        else if (other.gameObject.tag == "Gate")
        {
            //Debug.Log("Gate shot");
            other.gameObject.GetComponent<GateState>().locked = false;
            other.gameObject.GetComponent<Animator>().SetBool("GateLock", other.gameObject.GetComponent<GateState>().locked);
            other.gameObject.GetComponent<GateState>().RefreshCollider();

        }


    }
    
   
    private void LaunchCrystal(GameObject crystal)
    {
        float x = Random.Range(300, 500) * (Random.value >= 0.5f ? 1f: -1f);
        float y = Random.Range(300, 500);
        crystal.GetComponent<Rigidbody2D>().AddForce(new Vector2(x, y));

    }

    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
    }

    private void OnDestroy()
    {
        bt.missed = missed;
        
        
    }
    
}
