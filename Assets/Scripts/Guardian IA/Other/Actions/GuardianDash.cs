﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianDash : MonoBehaviour
{

    public bool turnOn;
    private Vector3 halfTarget;
    Quaternion theRotation;
    private float prevPos;

    // Update is called once per frame
    void Update()
    {

        if (turnOn)

            if ((transform.position - halfTarget).magnitude > 0.5f)
            {
                prevPos = transform.position.x;
                transform.position = Vector3.Lerp(transform.position, halfTarget, Time.deltaTime * transform.GetComponent<GridPathFinding>().speed * 2);
                if (!GetComponent<GridPathFinding>().facingRight && transform.position.x > prevPos || GetComponent<GridPathFinding>().facingRight && transform.position.x < prevPos)
                {
                    theRotation = transform.rotation;
                    theRotation.y += 180;
                    transform.rotation *= theRotation;
                    GetComponent<GridPathFinding>().facingRight = !GetComponent<GridPathFinding>().facingRight;
                }
            }
            else
                turnOn = false;


    }

    public void Dash(Vector3 target)
    {

        halfTarget = new Vector3((target.x + transform.position.x) / 2, (target.y + transform.position.y) / 2, 0f);
        turnOn = true;
    }
}
