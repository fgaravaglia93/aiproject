﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CRBT;

public class GuardianFSM : MonoBehaviour {
    [Range(0.5f, 1.5f)]
    public float reactionTime;


    //conditions 
    public float attackRange;
    public int currentInRoom;
    public bool havingCrystal = true;
    public bool heroClose = false;
    public bool foundGate = false;

    [HideInInspector]
    public GameObject crystal;
    [HideInInspector]
    public bool crystalEndDrop = false;
    [HideInInspector]
    public GameObject gate;


    private GameObject alchemist;
    private GameObject telekinetic;

    public Transform guardianSpawnPoint;
    public Transform crystalSpawn1;
    public Transform crystalSpawn2;
    public GameObject crystalPrefab;

    //Gun
    public GameObject bulletPrefab;
    private GameObject arm;
    private Transform bulletSpawn;
    //Debug distances
    private Vector2 direction;
    private Vector2 point;
    

    private GameObject target;


    private FSM fsm;
    private FSM fsmActive;
    //for hierarchical FSM (save state in active fsm when occurs active-->clear in upper level)
    private FSMState history;

    private DecisionTree chaseDt;
    private BehaviorTree attackBt;
    private int currentLevel = 0;



    // Use this for initialization
    void Start()
    {

        alchemist = GameManager.instance.alchemist;
        telekinetic = GameManager.instance.telekinetic;

        // Define states and add actions when enter/exit/stay
        FSMState inactive = new FSMState("inactive");

        //inactive.enterActions.Add(Stop);
        FSMState active = new FSMState("active");

        FSMState clear = new FSMState("clear");
        clear.stayActions.Add(ShootDownGate);

        // Define transitions "_a_" used wheter the condition consists in AND between conditions "_o_" for OR
        FSMAction[] fireInactiveChase = new FSMAction[1];
        fireInactiveChase[0] = WakeUp;
        FSMTransition t01 = new FSMTransition(StealCrystal,fireInactiveChase);

        FSMAction[] fireActiveClear = new FSMAction[1];
        fireActiveClear[0] = SaveHistory;
        FSMTransition t02 = new FSMTransition(Obstacle, fireActiveClear);

        FSMAction[] fireClearActive = new FSMAction[1];
        fireClearActive[0] = LoadHistory;
        FSMTransition t03 = new FSMTransition(FreePassage, fireClearActive);



        FSMState chase = new FSMState("chase");

        chase.stayActions.Add(Chase);
        chase.exitActions.Add(StopSeek);

        FSMState attack = new FSMState("attack");
        attack.enterActions.Add(ExtractGun);
        attack.enterActions.Add(AttackBehaviorTree);
        attack.exitActions.Add(HideGun);
        attack.exitActions.Add(StopAttackBehaviorTree);

        FSMState retrieve = new FSMState("retrieve");
        retrieve.enterActions.Add(RemoveCrystal);
        retrieve.stayActions.Add(ChaseCrystal);
        retrieve.exitActions.Add(StopSeek);

        // Define transitions "_a_" used wheter the condition consists in AND between conditions "_o_" for OR
        FSMTransition t11 = new FSMTransition(TargetClose);
        FSMTransition t12 = new FSMTransition(NoTargetClose);
        FSMTransition t13 = new FSMTransition(StealCrystal_a_TargetClose);
        FSMTransition t14 = new FSMTransition(DropCrystal_a_TargetClose);
        FSMTransition t15 = new FSMTransition(StealCrystal_a_NoTargetClose);
        FSMTransition t16 = new FSMTransition(DropCrystal);
        


        FSMAction [] fireRetrieveInactive = new FSMAction[4];
        fireRetrieveInactive[0] = ChangeFSMlevel;
        fireRetrieveInactive[1] = StopSeek;
        fireRetrieveInactive[2] = TeleportAtStart;
        fireRetrieveInactive[3] = RespawnCrystal;
        FSMTransition t017 = new FSMTransition(RetrieveCrystal, fireRetrieveInactive);


        // Link states with transitions

        inactive.AddTransition(t01, active);
        active.AddTransition(t02, clear);
        clear.AddTransition(t03, active);

        //this is activable only if currentState of "active" state is "retrieve"
        active.AddTransition(t017, inactive);

        chase.AddTransition(t11, attack);
        chase.AddTransition(t16, retrieve);
        attack.AddTransition(t12, chase);
        attack.AddTransition(t14, retrieve);
        retrieve.AddTransition(t13, attack);
        retrieve.AddTransition(t15, chase);
       

        // Setup a FSM at initial state
        fsm = new FSM(inactive, 1);
        fsmActive = new FSM(chase, 0);



        //Gun setup
        arm = transform.GetChild(0).gameObject;
        bulletSpawn = arm.transform.GetChild(0);
        arm.SetActive(false);

        //currentInRoom = 2;
        StartCoroutine(ControlState());

    }


    public IEnumerator ControlState()
    {
        while (true)
        {
            if(currentLevel == 0)
                Debug.Log("State "+fsm.current.stateName.ToUpper()+"\n");
            fsm.Update();
            
            currentLevel = 0;

            if (fsm.current.stateName == "active")
            {
                Debug.Log("State ACTIVE\nLow level state:"+ fsmActive.current.stateName.ToUpper());
                currentLevel = 1;
                fsmActive.Update();
               

            }

            yield return new WaitForSeconds(reactionTime);
        }
    }


    // CONDITIONS


    //CLEAR
    public bool Obstacle()
    {
        return foundGate;
    }


    public bool FreePassage()
    {
        Vector3 dir = gate.transform.position - transform.position;
        var hit = Physics2D.RaycastAll(transform.position, dir, dir.magnitude + 1f);
        foreach (var h in hit)
        {
            
            if (h.transform.tag == "Gate" && h.transform.parent.tag == "Opengate")
            {
                return false;
            }
                
        }
        foundGate = false;
        return true;
    }

    public void SaveHistory()
    {
        currentLevel = 0;
        history = fsmActive.current;
        

    }

    public void LoadHistory()
    {
        
        fsmActive.current = history;


    }


    //ATTACK
    public bool TargetClose()
    {
        return heroClose;
    }

    public bool NoTargetClose()
    {
        return !TargetClose();
    }

    
    //CHASE    
    public bool StealCrystal()
    {
        return alchemist.GetComponent<HeroStatus>().haveCrystal || telekinetic.GetComponent<HeroStatus>().haveCrystal;
 
    }


    //ATTACK
    public bool StealCrystal_a_TargetClose()
    {

        return (StealCrystal() && TargetClose());
    }

    public bool StealCrystal_a_NoTargetClose()
    {
        
        return (StealCrystal() && NoTargetClose());
    }


    //RETRIEVE
    public bool DropCrystal()
    {
        //Debug.Log("Crystal end drop" + crystalEndDrop);
        // return !StealCrystal() && crystalEndDrop;
        return crystalEndDrop;

    }

    //used for simulated the transition between two levels, transition level --> level 0 but the condition
    //is verified only if level1 state is the one from the transition can be fired
    public bool RetrieveCrystal()
    {
        return havingCrystal && fsmActive.current.stateName=="retrieve";
    }

    public bool DropCrystal_a_TargetClose()
    {
        return DropCrystal();

    }


    //ACTIONS
    public void WakeUp()
    {
        havingCrystal = false;
        // fsmRetrieve.history = fsmRetrieve.current ;

    }



    //Manage History and changing level
    public void ChangeFSMlevel()
    {
        currentLevel = 0;
        fsmActive.current = fsmActive.start;

    }


    public void TeleportAtStart()
    {
        print("teleport");
        transform.position = guardianSpawnPoint.position;
        currentInRoom = 5;
    }

    public void RespawnCrystal()
    {
        //print("respawn");
        var crystal = Instantiate(crystalPrefab, crystalSpawn1.position, Quaternion.identity);
        crystal.GetComponent<WaitToPick>().isPickable = true;
        var crystal2 = Instantiate(crystalPrefab, crystalSpawn2.position, Quaternion.identity);
        crystal2.GetComponent<WaitToPick>().isPickable = true;
        alchemist.GetComponent<HeroStatus>().haveCrystal = false;
        telekinetic.GetComponent<HeroStatus>().haveCrystal = false;
        alchemist.GetComponent<ManaBar>().healthBarSlider.value = 0f;
        telekinetic.GetComponent<ManaBar>().healthBarSlider.value = 0f;
    }



    //CHASE is a DT
    public void Chase()
    {
        //Decision tree
        chaseDt = GetComponent<GuardianChaseDT>().GetDt();
        chaseDt.Walk();
    }
    
    public void StopSeek()
    {
        GetComponent<GridPathFinding>().followPath = false;
        crystalEndDrop = false;
    }


    //ATTACK

    public void ExtractGun()
    {
        transform.GetChild(0).gameObject.SetActive(true);

    }

    public void AttackBehaviorTree()
    {
        StartCoroutine(GetComponent<CRBTGuardianAttack>().Attack());
    }

    public void StopAttackBehaviorTree()
    {
        Debug.Log("stop");
        GetComponent<CRBTGuardianAttack>().attack = false;
    }


    public void HideGun()
    {
        transform.GetChild(0).gameObject.SetActive(false);

    }



    public void ChaseCrystal()
    {
        if(crystal!=null)
            GetComponent<GridPathFinding>().Chase(crystal.transform);
    }

    public void ShootDownGate()
    {
        
        GetComponent<GuardianShoot>().FireStraight(gate.transform.position);
    }

    //RETRIEVE
    public void RemoveCrystal()
    {
        telekinetic.GetComponent<HeroStatus>().haveCrystal = false;
        alchemist.GetComponent<HeroStatus>().haveCrystal = false;
    }
}




