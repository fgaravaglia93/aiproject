﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CRBT;

public class CRBTGuardianAttack : MonoBehaviour {

    private BehaviorTree bt;

    public GameObject alchTarget;
    public GameObject telTarget;

    [HideInInspector]
    public bool attack = false;
    public bool missed = false;

    void Start()
    {


        BTAction a1 = new BTAction(ShootStraightBothToTarget);
        BTAction a2 = new BTAction(AimTowardsTarget);
        BTAction a3 = new BTAction(ShootStraightToTarget);
        BTAction a4 = new BTAction(DashTowardsTarget);
        BTAction a5 = new BTAction(LateralDash);
        
        

        BTCondition c1 = new BTCondition(BothTargetInAttackRange);
        BTCondition c2 = new BTCondition(TargetOnLineOfSight);
        BTCondition c3 = new BTCondition(TargetHit);

        BTSequence seq1 = new BTSequence(new IBTTask[] { c1, a1 });
        BTSequence seq3 = new BTSequence(new IBTTask[] { a5, a2, a3 });
        BTSequence seq5 = new BTSequence(new IBTTask[] { a3, c3 });

        BTDecorator d1 = new BTDecoratorUntilFail(seq5);
        BTSequence seq4 = new BTSequence(new IBTTask[] { a2, d1 });
        BTRandomSelector randSel2 = new BTRandomSelector(new IBTTask[] { seq4, a4 });
        BTSequence seq2 = new BTSequence(new IBTTask[] { c2, randSel2 });
        BTSelector sel1 = new BTSelector(new IBTTask[] { seq1, seq2, seq3 });

        bt = new BehaviorTree(sel1);
        
    }

    
    public BehaviorTree GetBt()
    {
        return bt;
    }


    //CONDITIONS
    public bool BothTargetInAttackRange()
    {
        
        if (alchTarget != null && telTarget != null)
        {
            Debug.Log("BT:\nBoth in range " + true);
            return true;
        }
        else
        {
            Debug.Log("BT:\nBoth in range " + false);
            return false;
        }
    }

    public bool TargetOnLineOfSight()
    {
        //LINE CAST

        /*if (alchTarget != null)
        {
            Debug.DrawLine(transform.position, alchTarget.transform.position, Color.green, 1f);
            if (Physics2D.Linecast(transform.position, alchTarget.transform.position, ~(1 << LayerMask.GetMask("Ground"))).collider == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        else
        {
            if (telTarget != null)
            {
                if (Physics2D.Linecast(transform.position, telTarget.transform.position, ~(1 << LayerMask.GetMask("Ground"))).collider == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }

        }*/

        if (alchTarget != null)
        {
            Debug.DrawLine(transform.position+ (new Vector3(0f,0.3f,0f)), alchTarget.transform.position + (new Vector3(0f, 0.3f, 0f)), Color.green, 1f);
            Debug.DrawLine(transform.position + (new Vector3(0f, -0.3f, 0f)), alchTarget.transform.position + (new Vector3(0f, -0.3f, 0f)), Color.green, 1f);
            if ((Physics2D.Linecast(transform.position + (new Vector3(0f, 0.3f, 0f)), alchTarget.transform.position + (new Vector3(0f, 0.3f, 0f)), ~(1 << LayerMask.GetMask("Ground"))).collider == null) &&
                (Physics2D.Linecast(transform.position + (new Vector3(0f, -0.3f, 0f)), alchTarget.transform.position + (new Vector3(0f, -0.3f, 0f)), ~(1 << LayerMask.GetMask("Ground"))).collider == null))
            {
                Debug.Log("BT:\nTarget in line of sight " + true);
                return true;
            }
            else
            {
                Debug.Log("BT:\nTarget in line of sight " + false);
                return false;
            }
        }

        else
        {
            if (telTarget != null)
            {
                Debug.DrawLine(transform.position + (new Vector3(0f, 0.3f, 0f)), telTarget.transform.position + (new Vector3(0f, 0.3f, 0f)), Color.green, 1f);
                Debug.DrawLine(transform.position + (new Vector3(0f, -0.3f, 0f)), telTarget.transform.position + (new Vector3(0f, -0.3f, 0f)), Color.green, 1f);
                if ((Physics2D.Linecast(transform.position + (new Vector3(0f, 0.3f, 0f)), telTarget.transform.position + (new Vector3(0f, 0.3f, 0f)), ~(1 << LayerMask.GetMask("Ground"))).collider == null) &&
                    (Physics2D.Linecast(transform.position + (new Vector3(0f, -0.3f, 0f)), telTarget.transform.position + (new Vector3(0f, -0.3f, 0f)), ~(1 << LayerMask.GetMask("Ground"))).collider == null))
                {
                    Debug.Log("BT:\nTarget in line of sight " + true);
                    return true;
                }
                else
                {
                    Debug.Log("BT:\nTarget in line of sight " + false);
                    return false;
                }

            }
            else
            {
                Debug.Log("BT:\nTarget in line of sight " + false);
                return false;
            }

        }

    }

    public bool TargetHit()
    {
        Debug.Log("BT:\n Target hit" + !missed);
        return !missed;
    }

    //ACTIONS
    public bool ShootStraightBothToTarget()
    {
        GetComponent<GuardianShoot>().FireCircular(8);
        Debug.Log("BT:\nShoot Both Circular");
        return true;
    }


    public bool AimTowardsTarget()
    {
        Debug.Log("BT:\nAim to target");
        if (alchTarget != null)
        {
            float angle = Mathf.Atan2((alchTarget.transform.position - transform.position).y, (alchTarget.transform.position - transform.position).x) * Mathf.Rad2Deg;

            transform.GetChild(0).GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, angle);

        }
        else
        {
            if (telTarget != null)
            {
                float angle = Mathf.Atan2((telTarget.transform.position - transform.position).y, (telTarget.transform.position - transform.position).x) * Mathf.Rad2Deg;

                transform.GetChild(0).GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, angle);

            }
            else
            {
                return false;
            }

        }


        return true;
    }

    public bool ShootStraightToTarget()
    {
       Debug.Log("BT:\n Shoot to target");
        if (alchTarget != null)
        {
            GetComponent<GuardianShoot>().FireStraight(alchTarget.transform.position);
        }

        else
        {
            if (telTarget != null)
            {
                GetComponent<GuardianShoot>().FireStraight(telTarget.transform.position);
            }
            else
            {
                return false;
            }

        }

        return true;
    }


    public bool DashTowardsTarget()
    {
        Debug.Log("BT:\n Dash towards target");
        if (alchTarget != null)
        {
            
            GetComponent<GuardianDash>().Dash(alchTarget.transform.position);
        }

        else
        {
            if (telTarget != null)
            {
                GetComponent<GuardianDash>().Dash(telTarget.transform.position);
            }
            else
            {
                return false;
            }

        }
        return true;
    }

    public bool RandomAim()
    {
        Vector3 randomTarget;
        randomTarget = Random.insideUnitCircle;
        float angle = Mathf.Atan2((transform.position - randomTarget).y, (transform.position - randomTarget).x) * Mathf.Rad2Deg;
        transform.GetChild(0).GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, angle);
        return true;
    }

    public bool LateralDash()
    {
        Vector3 distToTarget = new Vector3();
        Vector3 perpToTarget = new Vector3();
        Debug.Log("BT:\nLateral Dash");
        float degree = (Random.Range(0f, 1f) > 0.5f) ? 90f : -90f;
        if (alchTarget != null)
        {

            distToTarget = (alchTarget.transform.position - transform.position);
            Debug.DrawLine(transform.position, alchTarget.transform.position, Color.red, 2f);
            perpToTarget = Quaternion.Euler(0f, 0f, degree) * distToTarget;
            if(Physics2D.Linecast(transform.position, perpToTarget + transform.position, 1 << LayerMask.GetMask("Ground")).collider != null && alchTarget.GetComponent<HeroStatus>().currentInRoom == GetComponent<GuardianFSM>().currentInRoom)
            {
                perpToTarget = Quaternion.Euler(0f, 0f, -degree) * distToTarget;
            }
            Debug.DrawLine(transform.position, perpToTarget + transform.position, Color.red, 2f);
            //if (Physics2D.Linecast(transform.position, perpToTarget + transform.position, 1 << LayerMask.GetMask("Ground")).collider == null)
                GetComponent<GuardianDash>().Dash(perpToTarget + transform.position);
            
        }

        else
        {
            if (telTarget != null)
            {
                distToTarget = (telTarget.transform.position - transform.position);
                Debug.DrawLine(transform.position, telTarget.transform.position, Color.red, 2f);
                perpToTarget = Quaternion.Euler(0f, 0f, 90f) * distToTarget;
                if (Physics2D.Linecast(transform.position, perpToTarget + transform.position, 1 << LayerMask.GetMask("Ground")).collider != null && telTarget.GetComponent<HeroStatus>().currentInRoom == GetComponent<GuardianFSM>().currentInRoom)
                {
                    perpToTarget = Quaternion.Euler(0f, 0f, -degree) * distToTarget;
                }
                Debug.DrawLine(transform.position, perpToTarget + transform.position, Color.red, 2f);
                //if (Physics2D.Linecast(transform.position, perpToTarget + transform.position, 1 << LayerMask.GetMask("Ground")).collider == null)
                    GetComponent<GuardianDash>().Dash(perpToTarget + transform.position);
            }
            else
            {
                return false;
            }

        }
        return true;
    }

    public IEnumerator WaitToSeeCrystal()
    {
        yield return new WaitForSeconds(0.3f);
        GetComponent<GuardianFSM>().crystalEndDrop = true;
        
    }

    public IEnumerator Attack()
    {
        attack = true;
        while (attack)
        {
            GetBt().Step();
            yield return new WaitForSeconds(0.5f);
            
        }

    }


}
