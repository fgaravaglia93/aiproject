﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class GuardianChaseDT : MonoBehaviour {

    //[Range(0f, 20f)] public float range = 5f;

    //public float reactionTime = 3f;

    private GameObject alchemist;
    private GameObject telekinetic;

    private DecisionTree dt;
    public GameObject roomsNodes;

    private bool movingToARoom = false;
    private int lastIndexRoom;
    void Awake()
    {

       

        // Define actions
        DTAction a1 = new DTAction(ChaseTelekinetic);
        DTAction a2 = new DTAction(ChaseAlchemist);
        DTAction a3 = new DTAction(MoveToAdjacentRoom);

        // Define decisions
        DTDecision d1 = new DTDecision(MindTransferInactive);
        DTDecision d2 = new DTDecision(BothHeroesInRange);
        DTDecision d3 = new DTDecision(BothIntoAlchemist);
        DTDecision d4 = new DTDecision(TelekineticNearest);
        DTDecision d5 = new DTDecision(TelekineticInRange);
        DTDecision d6 = new DTDecision(AlchemistInRange);
        DTDecision d7 = new DTDecision(TelekineticInRange);

        // Link action with decisions
        d1.AddLink(true, d2);
        d1.AddLink(false, d3);

        d2.AddLink(true, d4);
        d2.AddLink(false, d5);

        d3.AddLink(true, d6);
        d3.AddLink(false, d7);

        d4.AddLink(true, a1);
        d4.AddLink(false, a2);

        d5.AddLink(true, a1);
        d5.AddLink(false, d6);

        d6.AddLink(true, a2);
        d6.AddLink(false, a3);

        d7.AddLink(true, a1);
        d7.AddLink(false, a3);

        // Setup my DecisionTree at the root node
        dt = new DecisionTree(d1);

    }

    void Start()
    {
        alchemist = GameManager.instance.alchemist;
        telekinetic = GameManager.instance.telekinetic;
    }
  

    // ACTIONS
    //a1
    public object ChaseTelekinetic(object o)
    {
        movingToARoom = false;
        GetComponent<GridPathFinding>().Chase(telekinetic.transform);
        Debug.Log("DT action:\nCHASE TELEKINETIC");
        return null;
    }

    // a2
    public object ChaseAlchemist(object o)
    {
        movingToARoom = false;
        GetComponent<GridPathFinding>().Chase(alchemist.transform);
        Debug.Log("DT action:\nCHASE ALCHEMIST");
        return null;
    }

    //a3
    public object MoveToAdjacentRoom(object o)
    {

       
        RoomNode[] nodes = roomsNodes.GetComponent<RoomsConnections>().GetAdjacents(GetComponent<GuardianFSM>().currentInRoom);

        

        if(nodes != null)
        {
            if (!movingToARoom)
            {
                movingToARoom = true;
                //pick random room in the adjacents
                int indexAdj = Random.Range(0, (nodes.Length - 1));
                lastIndexRoom = nodes[indexAdj].roomIndex;
                for (int i = 0; i < nodes.Length; i++)
                    if (nodes[i].roomIndex == alchemist.GetComponent<HeroStatus>().currentInRoom || nodes[i].roomIndex == telekinetic.GetComponent<HeroStatus>().currentInRoom)
                    {
                        lastIndexRoom = nodes[i].roomIndex;
                        indexAdj = i;
                        break;
                    }
            

                GetComponent<GridPathFinding>().Chase(nodes[indexAdj].sceneObject.transform);
                Debug.Log("DT Action:\nMOVE TO ADJACENT ROOM" + lastIndexRoom);
            } else
            {
                //GetComponent<GridPathFinding>().Chase(nodes[lastIndexRoom].sceneObject.transform);
                if (GetComponent<GuardianFSM>().currentInRoom == lastIndexRoom)
                {
                    movingToARoom = false;
                }
            }
            
         
           
            
        }
        
        return null;
    }

    // DECISIONS
    //d1
    public object MindTransferInactive(object o)
    {
        return !telekinetic.GetComponent<HeroStatus>().outOfBody && !alchemist.GetComponent<HeroStatus>().outOfBody;

    }

    // d2
    public object BothHeroesInRange(object o)
    {
        return (telekinetic.GetComponent<HeroStatus>().currentInRoom == transform.GetComponent<GuardianFSM>().currentInRoom) &&
        (alchemist.GetComponent<HeroStatus>().currentInRoom == transform.GetComponent<GuardianFSM>().currentInRoom);
    }

    // d3
    //Telekinetic casted Mind Transfer
    public object BothIntoAlchemist(object o)
    {
        return telekinetic.GetComponent<HeroStatus>().outOfBody;
    }

    public object TelekineticNearest(object o)
    {
        //check the nearest imply both heroes are in the same room
        int nodesToTel = GetComponent<GridPathFinding>().GetPath(transform, telekinetic.transform).Count;
        int nodesToAlch = GetComponent<GridPathFinding>().GetPath(transform, alchemist.transform).Count;
        return nodesToTel <= nodesToAlch;
    }

    //d5 and d7
    public object TelekineticInRange(object o)
    {

        return telekinetic.GetComponent<HeroStatus>().currentInRoom == transform.GetComponent<GuardianFSM>().currentInRoom;

    }

    //d6
    public object AlchemistInRange(object o)
    {
        return alchemist.GetComponent<HeroStatus>().currentInRoom == transform.GetComponent<GuardianFSM>().currentInRoom;
    }

    


    public DecisionTree GetDt()
    {
        if (dt != null)
            return dt;
        else
            print("vuoto");
        return null;
    }
}