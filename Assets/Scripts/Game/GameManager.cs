﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameObject telekinetic;
    public GameObject alchemist;
    public GameObject telUI;
    public GameObject alchUI;
    public GameObject controlsScreen;

    public bool paused;

    public static GameManager instance = null;

    
	// Use this for initialization
	void Awake () {

        if (instance == null)
            instance = this;

        if (instance != this)
            Destroy(gameObject);
    }
	
	// Update is called once per frame
	void Update () {

        if (!telekinetic.GetComponent<HeroStatus>().alive || !alchemist.GetComponent<HeroStatus>().alive)
        {
            //game over
            GetComponent<LoadSceneOnClick>().LoadByIndex(0);
        }

        if (Input.GetButtonDown("Escape"))
        {
            
            paused = !paused;
            telUI.SetActive(!paused);
            alchUI.SetActive(!paused);
            controlsScreen.SetActive(paused);

        }
     
            


    }


}
