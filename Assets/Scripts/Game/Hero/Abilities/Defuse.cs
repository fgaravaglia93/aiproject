﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defuse : MonoBehaviour {

    
    public bool abilityEffective = false;
    
   
    void Update()
    {
        if (Input.GetButtonDown("ShootingA") && abilityEffective)
        {
            transform.GetChild(2).GetComponent<HeroTriggerInteractions>().abilityInUse = true;
            
            
        } 
        if(Input.GetButtonDown("ShootingA") && !abilityEffective)
        {
            Debug.Log("Can't DEFUSE");
        }

    }
   
}
