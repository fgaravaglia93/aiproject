﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Move objects by telekinetic's ability.
/// </summary>


public class MoveObject : MonoBehaviour
{
    
    public int speed;
    public Camera cameraHero;

    private Rigidbody2D rb;
    private Transform rotation;
    private Vector2 normalizeDirection;
    private Vector2 pos;
    private Vector2 prevPos;
    private bool ability = false;
    private RaycastHit2D hit;
    private GameObject hitObj;
    private Animator animator;
    
    // Use this for initialization
    void Start()
    {

        //pos = Input.mousePosition;
        //pos = cameraHero.ScreenToWorldPoint(pos);
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {

        //check if an object has been clicked
        if (Input.GetMouseButtonDown(0) && !ability)
        {
            pos = Input.mousePosition;
            // Debug.Log("Pixel " + pos);
            pos = cameraHero.ScreenToWorldPoint(pos);
            // Debug.Log("World " + pos);

            hit = Physics2D.Raycast(pos, Vector2.zero);
            //Debug.Log(hit.transform);

            if (hit && hit.collider.gameObject.CompareTag("Movable"))
            {
                Debug.Log("f");
                //hit.collider.gameObject.GetComponent<MovableObj>().SetMovable(true);
                animator.SetBool("MoveObj", true);
                ability = true;
                GetComponent<SwitchAbility>().SetMovingObj(true);
                hitObj = hit.collider.gameObject;
                rb = hitObj.GetComponent<Rigidbody2D>();

            }

        }

        //calculate direction
        if (ability && Input.GetMouseButton(0)) {
           
            // Debug.Log(prevPos);
            prevPos = pos;
            pos = Input.mousePosition;
            pos = cameraHero.ScreenToWorldPoint(pos);
            hit = Physics2D.Raycast(pos, Vector2.zero);
            normalizeDirection = (pos - (Vector2)hitObj.transform.position).normalized;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            //Debug.Log(pos);
            rb.gravityScale = 0;
         }
    
        //stop ability
        if (ability && !Input.GetMouseButton(0)) {
            animator.SetBool("MoveObj", false);
            ability = false;
            rb.constraints = RigidbodyConstraints2D.None;
            rb.gravityScale = 1;
            GetComponent<SwitchAbility>().SetMovingObj(false);

        }
    }

    

        
    void FixedUpdate()
    {
        //move object
        if (ability && Input.GetMouseButton(0)) {
            if ((pos - prevPos).magnitude > 0.005f)
                //move taking count of global axes
                hitObj.transform.Translate(normalizeDirection * speed * Time.deltaTime, Space.World);
        }
    
       
        
    }
}