﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Shooting: MonoBehaviour
{

    public string shootingButton;
    public GameObject bulletPrefab;
    public GameObject arm;
    public Transform bulletSpawn;
    public float moveSpeed = 2;  // Units per second
    public Vector3 targetPos;
    private Quaternion armAngle;

    void Update()
    {
        if (Input.GetButtonDown(shootingButton))
        {
            //not shoot if Ui element is clicked
            //if(!EventSystem.current.IsPointerOverGameObject())
                Fire();
        }
    }


    void Fire()
    {
        
        // Create the Bullet from the Bullet Prefab
        
        var bullet = Instantiate(bulletPrefab, bulletSpawn.position, arm.transform.rotation);

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }
}