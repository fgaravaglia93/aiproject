﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Bullet : MonoBehaviour {

    public float speed = 10;
    public float damage = .3f;
    

    void OnCollisionEnter2D(Collision2D other)
    {
        Destroy(gameObject);
        /*if (other.gameObject.tag == "Enemy")
        {
            Slider enemyBar = other.gameObject.GetComponentInChildren<ManaRobot>().healthBarSlider;
            if (!enemyBar.gameObject.activeSelf)
            {
                enemyBar.gameObject.SetActive(true);
            }
            enemyBar.value -= damage;
            
        }*/
        /*if (other.gameObject.tag == "Hero")
        {
           Slider enemyBar = other.gameObject.GetComponentInChildren<ManaBar>().healthBarSlider;
            enemyBar.value -= damage;
        }*/
        //Debug.Log("Collision detected");
    }

    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
    }
}
