﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MindTransferInvoke : MonoBehaviour {


  
    public GameObject telekinetic;
    public GameObject alchemist;
    public GameObject telekineticSpirit;
    public GameObject alchemistSpirit;
    public GameObject ScreenFilterTel;
    public GameObject ScreenFilterAlch;
    public GameObject envAlch;

    //0.05
    public float manaLost;

    private GameObject currentHero;
    private string newAbility;
  
    //put the spirit behind player
    private int behind;
    private int secondSpiritPos= 1;
    private Slider heroBar;
    private Behaviour[] behavioursTel;
    private Behaviour[] behavioursAlch;
    private bool firstMind = false;


    void Start()
    {
        
        behavioursTel = telekinetic.GetComponentsInChildren<Behaviour>();
        behavioursAlch = alchemist.GetComponentsInChildren<Behaviour>();
    }


    void Update()
    {
        if (Input.GetButtonDown("MindTransferT") && !alchemist.GetComponent<HeroStatus>().outOfBody)
        {
            MindTransfer("Telekinetic");
            
        }

        if (Input.GetButtonDown("MindTransferA") && !telekinetic.GetComponent<HeroStatus>().outOfBody)
        {
            MindTransfer("Alchemist");
           
        }
        if (telekinetic.GetComponent<ManaBar>().manaZero)
        {
            MindTransfer("Telekinetic");
            telekinetic.GetComponent<ManaBar>().manaZero = false;
        }
        if (alchemist.GetComponent<ManaBar>().manaZero)
        {
            MindTransfer("Alchemist");
            alchemist.GetComponent<ManaBar>().manaZero = false;
        }
    }


    public void MindTransfer(string caster)
    {

        
        if (caster == "Telekinetic") {
            currentHero = telekinetic;
            telekinetic.GetComponent<HeroStatus>().outOfBody = !telekinetic.GetComponent<HeroStatus>().outOfBody;

            if (telekinetic.GetComponent<HeroStatus>().outOfBody && telekinetic.GetComponent<HeroStatus>().haveCrystal)
            {


                //GetComponent<SwitchListener>().SetMindTransfer(true);
                heroBar = telekinetic.GetComponent<ManaBar>().healthBarSlider;
                InvokeRepeating("LoseMana", 0, 0.5f);

                if (currentHero.GetComponent<Movement>().GetFacingRight())
                    behind = 1;
                else
                    behind = -1;

                telekinetic.GetComponent<Animator>().SetBool("OutOfBody", true);
                //telekinetic.GetComponent<ManaBar>().StartCoroutine(LooseMana);
                //Debug.Log("TELEKINETIC LEAVING BODY");
                // Instantiate(TelekineticSpirit, CurrentHero.transform.position + new Vector3(-1 * behind, 1, 0), Quaternion.identity, CurrentHero.transform);
                SwitchBehaviours(behavioursTel, false, ScreenFilterTel);

                alchemist.GetComponent<MoveObject>().enabled = true;
                alchemist.GetComponent<SwitchAbility>().SetIconGuestAbility("MoveObject");


            } else
            {

                telekinetic.GetComponent<Animator>().SetBool("OutOfBody", false);
                // Destroy(GameObject.FindWithTag("TelSpirit"));
                //Debug.Log("TELEKINETIC COMING BACK TO HER BODY");
                SwitchBehaviours(behavioursTel, true, ScreenFilterTel);
                alchemist.GetComponent<MoveObject>().enabled = false;
                telekinetic.GetComponent<MoveObject>().enabled = false;
                telekinetic.GetComponent<Defuse>().enabled = false;
                telekinetic.GetComponent<SwitchAbility>().ResetAbilities();
                alchemist.GetComponent<SwitchAbility>().SetIconGuestAbility("Null");
                CancelInvoke();
            }




        } else
        {
            currentHero = alchemist;
            
            alchemist.GetComponent<HeroStatus>().outOfBody = !alchemist.GetComponent<HeroStatus>().outOfBody;
           
           
            if (alchemist.GetComponent<HeroStatus>().outOfBody && alchemist.GetComponent<HeroStatus>().haveCrystal)
            {
                
               // GetComponent<SwitchListener>().SetMindTransfer(true);
                heroBar = alchemist.GetComponent<ManaBar>().healthBarSlider;
                InvokeRepeating("LoseMana", 0, 0.5f);

                if (currentHero.GetComponent<Movement>().GetFacingRight())
                    behind = 1;
                else
                    behind = -1;

                if (alchemist.GetComponent<HeroStatus>().outOfBody)
                    alchemist.GetComponent<Animator>().SetBool("OutOfBody", true);
              //  Instantiate(AlchemistSpirit, CurrentHero.transform.position + new Vector3(-1 * behind + secondSpiritPos, 1, 0), Quaternion.identity, CurrentHero.transform);

                //currentHero.GetComponent<SwitchAbility>().AddAbility("Defuse");
                //Debug.Log("AlCHEMIST LEAVING BODY");
                SwitchBehaviours(behavioursAlch, false, ScreenFilterAlch);
                telekinetic.GetComponent<Defuse>().enabled = true;
                telekinetic.GetComponent<SwitchAbility>().SetIconGuestAbility("Defuse");



            }
            else
            {
                alchemist.GetComponent<Animator>().SetBool("OutOfBody", false);
                //Destroy(GameObject.FindWithTag("AlchemistSpirit"));
                //Debug.Log("ALCHEMIST COMING BACK TO HIS BODY");
                SwitchBehaviours(behavioursAlch, true, ScreenFilterAlch);
                telekinetic.GetComponent<Defuse>().enabled = false;
                alchemist.GetComponent<MoveObject>().enabled = false;
                alchemist.GetComponent<Defuse>().enabled = false;
                alchemist.GetComponent<SwitchAbility>().ResetAbilities();
                CancelInvoke();
                telekinetic.GetComponent<SwitchAbility>().SetIconGuestAbility("Null");
            }

           
        }

        
        //start decrease mana on the player

        //activate new abilities on active hero

    }



    void SwitchBehaviours(Behaviour[] behaviours, bool enable, GameObject ScreenFilter)
    {
        if (behaviours != null)
        {
            foreach (Behaviour behaviour in behaviours)
            {
                //Check for generalize the collider
                // we can improve this part
                if ((behaviour.GetType() != Type.GetType("MeshRenderer")) &&
                    (behaviour.GetType() != typeof(PolygonCollider2D)) &&
                    (behaviour.GetType() != typeof(Animator)) &&
                    (behaviour.GetType() != Type.GetType("HeroStatus")) &&
                    (behaviour.GetType() != Type.GetType("ManaBar")))
                {
                   
                    behaviour.enabled = enable;
                }
               

            }

            ScreenFilter.SetActive(!enable);

        }

    }

    void LoseMana()
    {
        if (heroBar.value <= 1 && heroBar.value > 0)
        {
            heroBar.value -= manaLost;
        }
    }



}
