﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//<summary>
// Switch the current ability in  use during mind transfer.
// </summary>
public class SwitchAbility : MonoBehaviour {

    public List<string> abilities = new List<string>();
    //public string mainAbility;
    public string singularAbility;
    public string movableTag;
    public string switchAbility;
    public GameObject iconAbility;
    public GameObject iconGuestAbility;


    private Animator animator;
    private int i;
    private float val;
    private bool switchAxisInUse = false;
    private bool movingObj;
    private MonoBehaviour scriptName;
    private GameObject[] arrayMovable;

    public Sprite spriteShooting;
    public Sprite spriteMoveObject;
    public Sprite spriteDefuse;

	// Use this for initialization
	void Awake () {

        abilities.Add("Shooting");
        abilities.Add(singularAbility);
        animator = GetComponent<Animator>();
        i = 0;
        movingObj = false;

        
        iconAbility.GetComponent<Image>().sprite = spriteShooting;
        iconGuestAbility.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {


        val = Input.GetAxis(switchAbility);
        if (val != 0f && !switchAxisInUse && !movingObj)
        {
             switchAxisInUse = true;
             scriptName = (MonoBehaviour)GetComponent(abilities[i]);
             //Debug.Log(scriptName);
             scriptName.enabled = false;
             
             
             if(abilities[i] == "Shooting")
             {
                 transform.Find("Arm").gameObject.SetActive(false);
             }

             i++;

             if (i == abilities.Count)
                 i = 0;

             scriptName = (MonoBehaviour)GetComponent(abilities[i]);
            
             scriptName.enabled = true;

             if (abilities[i] == "Shooting" )
             {
                transform.Find("Arm").gameObject.SetActive(true);
                iconAbility.GetComponent<Image>().sprite = spriteShooting;

             }

            if (abilities[i] == "MoveObject")
            {
           
                iconAbility.GetComponent<Image>().sprite = spriteMoveObject;

            }

            if (abilities[i] == "Defuse")
            {

                iconAbility.GetComponent<Image>().sprite = spriteDefuse;

            }






        } else if (val == 0 && switchAxisInUse)
			switchAxisInUse = false;
    }



    //reset abilities for the freezed character, reset the current ability to "Shooting"
    public void ResetAbilities()
    {
      
            i = 0;
            scriptName = (MonoBehaviour)GetComponent(abilities[i]);
            scriptName.enabled = true;
            transform.Find("Arm").gameObject.SetActive(true);
            iconAbility.GetComponent<Image>().sprite = spriteShooting;
        
    }

    public void SetMovingObj(bool movingObj)
    {
        this.movingObj = movingObj;
    }

    public void SetIconGuestAbility(string ability)
    {
        switch (ability)
        {
            case "MoveObject":
                iconGuestAbility.SetActive(true);
                iconGuestAbility.GetComponent<Image>().sprite = spriteMoveObject;
                break;

            case "Defuse":
                iconGuestAbility.SetActive(true);
                iconGuestAbility.GetComponent<Image>().sprite = spriteDefuse;
                break;

            default:
                iconGuestAbility.GetComponent<Image>().sprite = null;
                iconGuestAbility.SetActive(false);
                break;
        }
    }
}
