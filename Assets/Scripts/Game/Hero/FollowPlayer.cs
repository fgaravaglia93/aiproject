﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FollowPlayer : MonoBehaviour{
    public Camera camera;
    Vector3 offset;

	// Use this for initialization
	void Start () {
        //for the alchemist camera set y=-0,13 and telekinetic y=0
        offset = camera.transform.position - transform.position;
        
	}
	
	// Update is called once per frame
	void Update () {

        camera.transform.position = transform.position + offset;
    }
}
