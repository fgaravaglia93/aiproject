﻿using UnityEngine;
using System.Collections;
public class Movement : MonoBehaviour {

    public string movementDirection;
    public string jump;
    public string hero;
    public float speed = 0.0f;
    public float lateralMovement;
	public float jumpMovement;


	public Transform groundCheck;

	public bool grounded = true;
    public SpriteRenderer[] armSprite;
    public GameObject arm;
    public GameObject characterGround;
    public GameObject bulletSpawn;
    
   
    private Animator animator;
    private Rigidbody2D rigidbody2d;
    private Quaternion armRotation;
    private bool facingRight;
    private int numOfJumps = 0;
    private int maxOfJumps = 2;
   

	// Use this for initialization
	void Start () {
        facingRight = true;
		animator = GetComponent<Animator> ();
		rigidbody2d = GetComponent<Rigidbody2D> ();
        armSprite = GetComponentsInChildren<SpriteRenderer>();
	}

    // Update is called once per frame
    void Update()
    {
        
       
        if (Input.GetButtonDown(jump))
        {
            grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

            if (grounded)
                numOfJumps = 0;
            if(grounded || numOfJumps < maxOfJumps) {
                if(numOfJumps<=0)
                    rigidbody2d.AddForce(Vector2.up * jumpMovement);
                else
                    rigidbody2d.AddForce(Vector2.up * jumpMovement/2);
                grounded = false;
                numOfJumps += 1;
            }
            

        }
            
        float val = Input.GetAxis(movementDirection);
        speed = lateralMovement * Mathf.Abs(Input.GetAxis(movementDirection));

            //Lo usa per andare a sinistra
       if ((hero == "Telekinetic") && ((val > 0 && !facingRight) || (val < 0 && facingRight)))
         {
           // Debug.Log(transform.GetChild(3).name);
            facingRight = !facingRight;
                Quaternion theRotation = transform.rotation;
                Transform[] children = new Transform[this.transform.childCount];
                int i = 0;
                foreach (Transform child in this.transform)
                {
                    children[i++] = child;
                }
                this.transform.DetachChildren();

                theRotation.y += 180;
                transform.rotation *= theRotation;
                armSprite[0].flipX = !armSprite[0].flipX;

                foreach (Transform child in children)
                {
                    child.parent = this.transform;
                }
                 //FLIP trigger
                 Quaternion theRotationtrigger = transform.GetChild(3).transform.rotation;
                
                 theRotationtrigger.y += 180;
            
                transform.GetChild(3).transform.rotation *= theRotationtrigger;
                children = null;
                arm.transform.localScale = new Vector3(1f, 1f, 1f);
           
        }

        //temporary for flip alchemist
        if ((hero == "Alchemist") && ((val > 0 && !facingRight) || (val < 0 && facingRight)))
        {
                
                facingRight = !facingRight;
                GetComponent<AimMovement>().SetFacingRight(facingRight);
                armSprite[0].flipX = !armSprite[0].flipX;
                Quaternion theRotation = transform.rotation;
                theRotation.y += 180;
                transform.rotation *= theRotation;
                GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
        }

            //spostati di velocità
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            //per interagire con l'animator, prendi il valore assoluto
            animator.SetFloat("Speed", Mathf.Abs(speed));

        
    }


    public bool GetFacingRight()
    {
        return facingRight;
    }

    }