﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this script is designed for Alchemist shoot aiming
public class AimMovement : MonoBehaviour {


    
    float rotateSpeed = 400f;
    bool facingRight = true;
    Transform arm;
    // Use this for initialization
    void Start () {
   
        arm = this.gameObject.transform.GetChild(0);
	}
	
	// Update is called once per frame
	void Update () {

        float val = Input.GetAxis("Aim");

        //TO CHANGE
        
            if (val > 0 && (((arm.rotation.z < 0.7f) && facingRight) || ((arm.rotation.z < 0.007f) && !facingRight)))
            {
                arm.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
                //Debug.Log(arm.rotation.z);
            }
            if (val < 0 && (((arm.rotation.z > -0.7f) && facingRight) || ((arm.rotation.z > -0.007f) && !facingRight)))
            {
                arm.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime * (-1));
                //Debug.Log(arm.rotation.z);
            }


    }

    public void SetFacingRight(bool facingRight)
    {
        this.facingRight = facingRight;
    }
}
