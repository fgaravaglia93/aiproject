﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRotation : MonoBehaviour {
    public GameObject arm;
    Vector2 mouse;
    Vector2 armPosition;
    Vector2 offset;
    Quaternion theRotationtrigger;
    float angle;
    public Camera heroCamera;
    private bool front = true;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        

            mouse = Input.mousePosition;
            armPosition = heroCamera.WorldToScreenPoint(transform.position);
            offset = new Vector2(mouse.x - armPosition.x - transform.position.x, mouse.y - armPosition.y - transform.position.y);
            //Debug.Log("Offset x "+transform.parent.position.x +"- Char position" +transform.parent.position);

            //find angle of rotation
            angle = Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg;
            if ((angle >= 90 || angle <= -90) && front == true)
            {
                front = false;
                GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
                if (GetComponentsInChildren<SpriteRenderer>().Length > 1)
                {
                    arm.GetComponent<SpriteRenderer>().flipY = !arm.GetComponent<SpriteRenderer>().flipY;
                }

                //FLIP trigger
                theRotationtrigger = transform.GetChild(2).transform.rotation;
                theRotationtrigger.y += 180;
                transform.GetChild(2).transform.rotation *= theRotationtrigger;
        }
            if ((angle <= 90 && angle >= -90) && front == false)
            {
                front = true;
                GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
                if (GetComponentsInChildren<SpriteRenderer>().Length > 1)
                {
                    arm.GetComponent<SpriteRenderer>().flipY = !arm.GetComponent<SpriteRenderer>().flipY;
                }
               
                //FLIP trigger
                theRotationtrigger = transform.GetChild(2).transform.rotation;
                theRotationtrigger.y += 180;
                transform.GetChild(2).transform.rotation *= theRotationtrigger;
            
        }
            arm.GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, angle);
        
    }
}
