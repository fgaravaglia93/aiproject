﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianSeek : MonoBehaviour {

    public bool turnOn = false;
    public float speed; //default 20
    private float prevAngle;
    private Transform target;

    public GameObject  telekinetic;
    

	// Use this for initialization
	void Start () {
        prevAngle = 0f;
	}
	
	// Update is called once per frame
	void Update () {

        if (turnOn)
        {
            Seek();
        }
	}

    public void SetTarget(Transform target)
    {
        this.target = target;
        prevAngle = Vector3.Angle(transform.GetChild(1).position, target.position);
    }

    void Seek()
    {

        target = telekinetic.transform;
        Vector2 velocity = (target.position - transform.position).normalized* speed;
        GetComponent<Rigidbody2D>().AddForce(velocity * Time.deltaTime);
        Vector3 appo = transform.GetChild(1).position;
        Vector3 appo0 = transform.position;
        float angle = Vector3.Angle(appo, target.position);
        print(angle);
        if(angle> 3)
         //   transform.GetChild(1).RotateAround(transform.position, Vector3.forward , angle);
        prevAngle = angle;
    }
}
