﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroTriggerInteractions : MonoBehaviour {

    [HideInInspector]
    public bool abilityInUse;
    [HideInInspector]
    public bool firstTime = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Crystal" && collision.gameObject.GetComponent<WaitToPick>().isPickable)
        {
            //print("Ho raccolto il cristallo");
            transform.parent.GetComponent<ManaBar>().healthBarSlider.value = 1f;
            transform.parent.GetComponent<HeroStatus>().haveCrystal = true;
            Destroy(collision.gameObject);
        }

        if(collision.gameObject.tag == "Gate" && transform.parent.GetComponent<Defuse>().enabled)
        {
            transform.parent.GetComponent<Defuse>().abilityEffective = true;
        }



    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        GameObject gate;
        if (collision.gameObject.tag == "Gate" && transform.parent.GetComponent<Defuse>().enabled)
        {
            
            if (abilityInUse)
            {
                gate = collision.gameObject;
                //print("defuse");
                if(collision.gameObject.GetComponent<GateState>() == null)
                {
                    gate = gate.transform.parent.gameObject;
                }
                gate.GetComponent<GateState>().locked = !gate.GetComponent<GateState>().locked;
                gate.GetComponent<Animator>().SetBool("GateLock", gate.GetComponent<GateState>().locked);
                gate.GetComponent<GateState>().RefreshCollider();
                //firstTime = false;
                abilityInUse = false;
            }
        }
    }


}
