﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour
{

    public Slider healthBarSlider;
    public bool manaZero = false;
    private float count;
    

    private void Update()
    {
        if (GetComponent<HeroStatus>().outOfBody)
        {
            count += Time.deltaTime;
            if(count > 5)
            {
                healthBarSlider.value -=  0.1f;
                count = 0;
            }

            if (healthBarSlider.value <= 0.1f)
                manaZero = true;
           
        }

        
    }

    //if player is not dead nor full health heal him
    void RefillMana()
    {
        if (healthBarSlider.value < 1 && healthBarSlider.value > 0)
        {
            healthBarSlider.value += .011f;  //increase health
        }
    }



}